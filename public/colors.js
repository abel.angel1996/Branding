export const colores =
{
  /* modo claro (mc) */

  /** titulos (title) **/
  'plt-mc-title-A': '#2A3B47',

  /** textos (txt) **/
  'plt-mc-txt-A': '#697477',
  'plt-mc-txt-B': '#A0A7AC',

  /** bordes (border) **/
  'plt-mc-border-A': '#EFF3F5',

  /** backgrounds **/
  'plt-mc-body-A': '#FBFBFE',
  'plt-mc-box-A': '#FFFFFF',
  /* 'plt-mc-color-A':'#BEF264', */ 
  'plt-mc-color-A':'#4dc173',

  /* modo oscuro (mo) */

  /** titulos (title) **/
  'plt-mo-title-A': '#EFF3F5',

  /** textos (txt) **/
  'plt-mo-txt-A': '#C8CDD0',
  'plt-mo-txt-B': '#A0A7AC',

  /** bordes (border) **/
  'plt-mo-border-A': '#2A3B47',

  /** backgrounds **/
  'plt-mo-body-A': '#192229',
  'plt-mo-box-A': '#212E36',
  'plt-mo-color-A':'#C1FF72'
}
