import data from './mvvSuperior.json'

export const Superior = () => {
  return (
    <section id='legend' className='flex justify-center w-full bg-palette-light-bg200 dark:bg-palette-light-txt800'>
      <div className='col-span-3 flex items-center justify-between  h-[30vh] w-10/12'>
        <div className='text-center w-[45%] border-r-2 h-3/5'>
          <h2 className='text-[1.2vw] p-0 mt-11 text-palette-light-txt800 dark:text-palette-light-bg100'>{data.title}</h2>
        </div>
        <div className='w-1/2 p-3'>
          <p className='text-[0.9vw] mb-3 text-palette-light-txt800 dark:text-palette-light-bg100'>{data.p1}</p>
          <p className='text-[0.9vw] mt-3 text-palette-light-txt800 dark:text-palette-light-bg100'>{data.p2}</p>
        </div>
      </div>
    </section>
  )
}
