import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// la secciones tiene planes //
// cada plan tiene items     //

// crea la seccion en general
export function Plan02 () {
  return (
    <section id='plan01' className='grid place-content-center w-full h-screen'>
      <h1 className='w-full text-center'> titulo </h1>
      <div id='plan01-plans' className='w-full h-4/5'>
        <section id='plan01-plan0' className='h-4/5 w-1/4 flex flex-wrap justify-center items-center'>
          <h2 id='plan01-plan0-title' className='w-full text-center text-2xl'> asdqwdqwd </h2>
          <div className='w-full items-center'>
            <p className='w-1/2 text-center text-4xl'> asdasdd </p>
            <p className='w-1/2 text-center text-xs'> adasdwq </p>
          </div>
          <p className='w-full text-center text-xs'> asdadd </p>
          <button className='w-4/5 border rounded text-xs text-center'> asdasdas </button>
          <ul className='w-full text-center'>
            <li className='w-full'>
              <FontAwesomeIcon icon='fa-duotone fa-circle-check' />
              <p className='text-xs'> asdasd </p>
            </li>
          </ul>
          <a href='' className='text-center'> asdasdadsa </a>
        </section>
      </div>
    </section>
  )
}
