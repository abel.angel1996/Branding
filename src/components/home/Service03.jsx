import boxes from './service03-vars.json'
import cnst from './service03-const.json'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faNewspaper, faGear, faGears, faBug } from '@fortawesome/free-solid-svg-icons'
library.add(faNewspaper, faGear, faGears, faBug)

export function Service03 () {
  return (
    <section id='service' className='w-full flex flex-wrap justify-center bg-white items-center py-20'>
      <div id='service-titleContainer' className='w-full flex justify-center items-center'>
        <h2 className='text-palette-light-txt800 dark:text-palette-light-bg100 text-4xl mb-14 w-1/2 text-center'> {cnst.title} </h2>
      </div>
      <div id='service-boxes' className='w-full h-auto flex flex-wrap justify-evenly items-center'>
        {        
          BoxesMap()
        }
      </div>
    </section>
  )
}

function BoxesMap() {

  let derecha = true;
  let box;

  return boxes.map( box => {
    
    derecha ? box = <Box key={box.id} data={box} /> : box = <Box2 key={box.id} data={box} />
    derecha = !derecha;
    return (box)
  
  })

}

function Box ({ data }) {
  return (
    <div id={`service-box0${data.id}`} className='w-full h-[70vh] flex justify-evenly items-center px-10 py-20 bg-plt-mc-box-A dark:bg-plt-mo-box-A '>
      <div className='w-1/3 h-full flex justify-evenly items-center flex-col'>
        <b id={`service-box0${data.id}-textBold`} className='flex text-center justify-center items-center h-1/6 text-palette-light-txt800 dark:text-palette-light-bg100 text-5xl'>  {data.textBold} </b>
        <p id={`service-box0${data.id}-text`} className='flex text-center h-3/6 text-palette-light-txt800 dark:text-palette-light-bg100 text-lg'>  {data.text} </p>
      </div>
      <img src={data.icon} className='w-1/3 h-full shadow-lg m-0 p-0 rounded-lg'></img>
    </div>
  )
}

function Box2 ({ data }) {
  return (
    <div id={`service-box0${data.id}`} className='w-full h-[70vh] flex justify-evenly items-center px-10 py-20 bg-plt-mc-box-A dark:bg-plt-mo-box-A '>
      <img src={data.icon} className='w-1/3 h-full m-0 p-0 rounded-lg shadow-lg'></img> 
      <div className='w-1/3 h-full flex justify-evenly items-center flex-col'>
        <b id={`service-box0${data.id}-textBold`} className='flex text-center justify-center items-center h-1/6 text-palette-light-txt800 dark:text-palette-light-bg100 text-5xl'>  {data.textBold} </b>
        <p id={`service-box0${data.id}-text`} className='flex text-center h-3/6 text-palette-light-txt800 dark:text-palette-light-bg100 text-lg'>  {data.text} </p>
      </div>
    </div>
  )
}
