import { T1 } from '../../atoms/T1';
import h from './hero-const.json';
import Typewriter from 'typewriter-effect';
import {TextAnimated} from '../../atoms/TextAnimated'

export function Hero () {
  return (
    <div id="hero01" className="w-full h-screen py-20 sm:h-[700px] flex flex-wrap text-palette-light-txt800 dark:text-palette-light-bg100 bg-[url('./src/assets/img/vectorizado.png')] bg-cover bg-no-repeat bg-center bg-slate-500">
      <div id="hero01-inner" className='py-[100px] w-full h-auto flex justify-center items-center flex-wrap'>
        <ul id="hero01-inner" className='flex flex-col justify-center items-center flex-wrap w-4/5 gap-[30px]'>
          <T1 id="hero01-title"> { h.title } </T1>
          <li id="hero01-description" className='text-center font-semibold text-xl w-11/12 lg:w-3/5'> { animation() } </li>
          <li id="hero01-buttons" className='w-full h-auto flex justify-center flex-wrap'>
            <div id="hero01-buttonsInner" className='w-1/2 h-auto flex items-center flex-nowrap justify-evenly gap-4'>
              <button id="hero01-buttonA" className='text-inherit bg-plt-mc-color-A hover:bg-palette-light-txt700 dark:hover:bg-palette-light-bg100 dark:hover:text-palette-light-txt800 rounded-[100px] py-3 px-5 font-semibold cursor-pointer text-sm sm:text-base sm:min-w-[145px] min-w-[100px] '> {h.buttonA} </button>
              <button id="hero01-buttonB" className='text-inherit border-[#282c34] hover:bg-palette-light-txt700 dark:hover:bg-palette-light-bg100 dark:hover:text-palette-light-txt800 border-[1px] rounded-[100px] py-3 px-5 font-semibold cursor-pointer text-sm sm:text-base sm:min-w-[145px] min-w-[100px]'> {h.buttonB} </button>
            </div>
          </li>
        </ul>
      </div>
    </div>
  )
}

const animation = () => {
  return (
    <Typewriter
      onInit={
        (typewriter) => {
          typewriter
            .pauseFor(1500)
            .typeString(h.description)
            .start()
        }
      }
      options={{
        autoStart: true,
        loop: false,
        delay: 45
      }}
    />
  )
}
