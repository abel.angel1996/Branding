import boxes from './service02-vars.json'
import cnst from './service02-const.json'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faNewspaper, faGear, faGears, faBug } from '@fortawesome/free-solid-svg-icons'
library.add(faNewspaper, faGear, faGears, faBug)

export function Service02 () {
  return (
    <section id='service' className='w-full flex flex-wrap justify-center items-center bg-slate-100 py-20'>
      <div id='service-titleContainer' className='w-full flex justify-center items-center flex-col'>
        <h2 className='text-palette-light-txt800 dark:text-palette-light-bg100 text-2xl mb-5 w-1/2 text-center font-bold'> {cnst.title} </h2>
        <h3 className='text-palette-light-txt700 dark:text-palette-light-bg100 text-xl mb-14 w-1/2 text-center'> {cnst.subtitle} </h3>

      </div>
      <div id='service-boxes' className='w-full h-[50vh] flex justify-evenly items-center'>
        {boxes.map(box => (
          <Box key={box.id} data={box} />
        ))}
      </div>
    </section>
  )
}

function Box ({ data }) {
  return (
    <div id={`service-box0${data.id}`} className='w-1/5 h-[50vh] flex flex-col justify-evenly items-center px-10 py-5 border border-plt-mc-border-A bg-plt-mc-box-A dark:bg-plt-mo-box-A shadow-2xl hover:border-plt-mc-color-A'>
      <div id={`service-box0${data.id}-iconContainer`} className='w-1/2 h-2/6 flex items-center justify-center'>
        <FontAwesomeIcon id={`service-box0${data.id}-icon`} className='h-1/2 p-5 rounded-full bg-palette-light-bg400 dark:bg-palette-light-txt800' icon={data.icon} />
      </div>
      <b id={`service-box0${data.id}-textBold`} className='flex text-center justify-center items-center h-1/6 text-palette-light-txt800 dark:text-palette-light-bg100'>  {data.textBold} </b>
      <p id={`service-box0${data.id}-text`} className='flex text-center h-3/6 text-palette-light-txt800 dark:text-palette-light-bg100'>  {data.text} </p>
    </div>
  )
}
