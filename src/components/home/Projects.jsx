import { useState } from 'react'

import projects from './projects.json'
import logoSeguridad from '../../assets/img/seguridad.svg'
import logoEducation from '../../assets/img/educacion.svg'
import logoImpositivo from '../../assets/img/impositivo.svg'
import logoSalud from '../../assets/img/salud.svg'

const urlImagenes = [logoSeguridad, logoImpositivo, logoSalud, logoEducation, logoSeguridad]

export function Projects () {
  const [contenidoRecuadro, setContenidoRecuadro] = useState(<ContenidoDetalleProyecto titulo={projects[0].titulo} descripccion={projects[0].description} imagen={urlImagenes[0]} />)
  const [cajaIluminada, setCajaIluminada] = useState(0)

  // Actualiza el componente en la caja de detalle de la sección proyectos, llama a la función para manejar la iluminación
  const manejarClick = (componente, index) => {
    setContenidoRecuadro(componente)
    setCajaIluminada(index)
  }

  return (
    <section className='w-full py-10 flex items-center justify-center bg-palette-light-bg100 dark:bg-palette-light-txt700'>
      <ul className='w-5/6 flex flex-wrap justify-between items-center'>
        <li className='flex flex-col gap-5'>
          <CreateBoxes cajaIluminada={cajaIluminada} manejarClick={manejarClick} />
        </li>
        <li className='w-3/5 h-full items-center justify-center rounded-lg  bg-palette-light-bg200 dark:bg-palette-light-txt800'>
          {contenidoRecuadro}
        </li>
      </ul>
    </section>
  )
};

const CreateBoxes = (props) => {
  return projects.map((p, key) => {
    return (
      <div key={key} className={`w-[10vw] h-[10vw] rounded-md bg-palette-light-bg200 dark:bg-palette-light-txt800 flex-col items-center justify-evenly flex cursor-pointer ${props.cajaIluminada === key ? 'shadow-[5px_5px_12px_0_rgba(255,255,255,1)]' : 'shadow-[5px_5px_12px_0_rgba(255,255,255,0.01)]'}`} onClick={() => props.manejarClick(<ContenidoDetalleProyecto titulo={p.titulo} descripccion={p.description} imagen={urlImagenes[p.id]} />, key)}>
        {contenidoCajasSeccionProyectos(urlImagenes[p.id], p.titulo)}
      </div>
    )
  })
}

// Proposito: dada un imagen, un titulo y una descripcion crea el contenido de la caja de detalle de la seccion proyectos.

const ContenidoDetalleProyecto = (props) => {
  return (
    <section id='DetalleProyecto' className='flex h-full flex-row wrap justify-evenly'>
      <div id='DetalleProyecto--derecha' className='w-auto h-full flex justify-center items-center py-0 px-[5vw]'>
        <div id='DetalleProyecto--derecha--contenedorDeImagen' className='w-auto h-[30%]'>
          <img id='DetalleProyecto--Icon' src={props.imagen} className='invert-[95%] sepia-[42%] saturate-[5365%] hue-rotate-15 brightness-[106] contrast-[107%]' alt='proyecto-icono' />
        </div>
      </div>
      <div id='DetalleProyecto--izquierda' className='h-full w-[65%] flex flex-wrap content-center gap-[2vh]'>
        <div id='DetalleProyecto--up' className='w-full h-auto'> <h6 className='text-palette-light-txt800 dark:text-palette-light-bg100'>{props.titulo}</h6> </div>
        <div className='w-full h-auto'> <p className='text-palette-light-txt800 dark:text-palette-light-bg100'>{props.descripccion}</p> </div>
      </div>
    </section>
  )
}

// Proposito: dada un titulo e imagen crea el contenido de una caja de la seccion proyectos.

const contenidoCajasSeccionProyectos = (logo, titular) => {
  return (
    <div id='proyectos--box' className='w-[10vw] h-[10vw] rounded-md bg-palette-light-bg200 dark:bg-palette-light-txt800 flex-col items-center justify-evenly flex cursor-pointer'>
      <div id='proyectos--box--contenedorDeImagen' className='w-1/4 h-1/4'><img className='invert-[95%] sepia-[42%] saturate-[5365%] hue-rotate-15 brightness-[106] contrast-[107%]' src={logo} alt='proyecto-icono' /></div>
      <div id='proyectos--box--titulo' className='w-auto h-auto'><p className='text-palette-light-txt800 dark:text-palette-light-bg100'>{titular}</p></div>
    </div>
  )
}
