export function Mision () {
    return (
        <section id="service" className="w-full flex justify-center items-center bg-palette-light-bg200 dark:bg-palette-light-txt800 h-screen">
            <h1 className="text-palette-light-txt800 dark:text-palette-light-bg100 text-6xl">Mision</h1>
        </section>
    )
}