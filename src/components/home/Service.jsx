import boxes from './service-vars.json'
import cnst from './service-const.json'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faNewspaper, faGear, faGears, faBug } from '@fortawesome/free-solid-svg-icons'
library.add(faNewspaper, faGear, faGears, faBug)

export function Service () {
  return (
    <section id='service' className='w-full flex flex-wrap justify-center items-center bg-palette-light-bg100 dark:bg-palette-light-txt700 h-[100vh] py-10'>
      <div id='service-titleContainer' className='w-full flex justify-center items-center'>
        <h2 className='text-palette-light-txt800 dark:text-palette-light-bg100 text-6xl'> {cnst.title} </h2>
      </div>
      <div id='service-boxes' className='w-full h-[50vh] flex justify-evenly items-center'>
        {boxes.map(box => (
          <Box key={box.id} data={box} />
        ))}
      </div>
    </section>
  )
}

function Box ({ data }) {
  return (
    <div id={`service-box0${data.id}`} className='w-1/6 h-[50vh] flex flex-col justify-evenly items-center px-1 py-2'>
      <div id={`service-box0${data.id}-iconContainer`} className='w-1/2 h-2/6 flex items-center justify-center'>
        <FontAwesomeIcon id={`service-box0${data.id}-icon`} className='h-1/2 p-5 rounded-full bg-palette-light-bg400 dark:bg-palette-light-txt800' icon={data.icon} />
      </div>
      <b id={`service-box0${data.id}-textBold`} className='flex text-center justify-center items-center h-1/6 text-palette-light-txt800 dark:text-palette-light-bg100'>  {data.textBold} </b>
      <p id={`service-box0${data.id}-text`} className='flex text-center h-3/6 text-palette-light-txt800 dark:text-palette-light-bg100'>  {data.text} </p>
    </div>
  )
}
