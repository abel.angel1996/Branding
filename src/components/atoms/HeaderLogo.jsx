import { Link } from 'react-router-dom'

export const HeaderLogo = () => {
  return (
    <div className='flex items-center justify-center'>
      <Link to='/#topComponent' className='no-underline flex items-end flex-row'>
        <h1 className='p-0 text-[#484c52] dark:text-[#ffffff] cursor-pointer select-none text-[2.5rem] h-full flex items-end flex-row'> 
          Ørbital
          <p className='p-0 m-0 text-xs pb-3'> Co.</p>
        </h1>
        
      </Link>
    </div>
  )
}
