import { useState } from 'react'

export const HeaderItemSimple = ({ data }) => {
  const [popUp, setPopUp] = useState(false)

  const handlePopUp = () => {
    setPopUp(!popUp)
  }

  return (
    <li className='flex h-1/2 cursor-pointer' onMouseEnter={handlePopUp} onMouseLeave={handlePopUp}>
      <h4 className='border-none text-[#484c52] dark:text-[#ffffff] text-xl tracking-[2px] font-bold relative cursor-pointer select-none h-6 top-[10px] no-underline '>
        {data.title}
      </h4>
    </li>
  )
}
