import { Home } from './pages/Home'
import { Home2 } from './pages/Home2'

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'

function App () {
  return (
    <div className='w-full'>
      <Router>
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/home2' element={<Home2 />} />
        </Routes>
      </Router>
    </div>
  )
};

export default App
