import { useState } from 'react'

export const PortalBox = ({ colorSection, id, colorArr, setColorArr }) => {
  const [color, setColor] = useState('')

  const handleChange = (e) => {
    setColor(e.target.value)
  }

  const handleClick = (color) => {
    const updatedColorArr = [...colorArr]
    updatedColorArr.splice(id, 1, color)
    setColorArr(updatedColorArr)
    // const updatedColorArr = colorArr.map(inputValor => {
    //   if (inputValor.id === id) {
    //     return {
    //       ...inputValor,
    //       color
    //     }
    //   } else {
    //     return inputValor
    //   }
    // })
    // const foundId = updatedColorArr.some(inputValor => inputValor.id === id)
    // if (!foundId) {
    //   updatedColorArr.splice(id, 0, color)
    // }
  }

  return (
    <div id={`portalBox0${id}`} className='flex w-9/12 bg-bgColor dark:bg-bgColor py-1 px-2 border rounded-xl flex-row justify-evenly items-center border-black box-border text-txtColor'>
      <p id={`portalBox0${id}-text`} className='w-1/2'>{colorSection}:</p>
      <input id={`portalBox0${id}-input`} onChange={handleChange} type='text' className='rounded-xl border w-3/12' />
      <button id={`portalBox0${id}-button`} onClick={() => handleClick(color)} type='button' className='rounded-xl border w-1/12'>AGREGAR</button>
      <div id={`portalBox0${id}-colorContainer`} className='p-0.5 border w-7 h-7 border-black rounded '>
        <div id={`portalBox0${id}-color`} className='w-full h-full rounded' style={{ backgroundColor: color }}> </div>
      </div>
    </div>
  )
}
