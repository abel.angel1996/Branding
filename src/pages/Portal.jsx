import { PortalBox } from './PortalBox'
import { useState, useEffect } from 'react'
import colors from './portal.json'

export function Portal () {
  const [colorArr, setColorArr] = useState(() => {
    const storedColorArr = window.localStorage.getItem('colorArr')
    return storedColorArr ? JSON.parse(storedColorArr) : Array(3).fill('')
  }
  )

  useEffect(() => {
    window.localStorage.setItem('colorArr', JSON.stringify(colorArr))
  }, [colorArr])

  const handleSubmit = (e) => {
    e.preventDefault()
    console.log(colorArr)
    for (let i = 0; i < colors.length; i++) {
      const colorVar = colorArr[i]
      document.documentElement.style
        .setProperty(colors[i].colorValue, colorVar)
      console.log(colors[i].colorValue)
    }
  }

  return (
    <section id='Portal' className='bg-bgColorContainer dark:bg-bgColorContainer flex w-full justify-center items-center '>
      <form onSubmit={handleSubmit} className='w-1/2 h-2/3 flex flex-col gap-4 items-center rounded-xl py-8 border border-black box-border'>
        {colors.map(color => {
          return (
            <PortalBox
              key={color.id}
              id={color.id}
              colorSection={color.name}
              colorValue={color.colorValue}
              colorArr={colorArr}
              setColorArr={setColorArr}
            />
          )
        })}
        <div className='flex w-9/12 h-auto py-1 px-2 border rounded-xl flex-row justify-end'>
          <button type='submit' className='p-1 border rounded-xl w-1/3 h-full border-black'>Submit</button>
        </div>
      </form>
    </section>
  )
}
