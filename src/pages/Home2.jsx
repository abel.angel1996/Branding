import { Portal } from './Portal.jsx'
import { Footer } from '../components/commons/Footer.jsx'

export const Home2 = () => {
  return (
    <div className='bg-[#eeeeee] dark:bg-[#282c34]'>
      <Portal />
      <Footer />
    </div>
  )
}
