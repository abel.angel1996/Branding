import { Header } from '../components/commons/Header.jsx'
import { Hero } from '../components/home/Hero.jsx'
import { Service02 } from '../components/home/Service02.jsx'
import { Superior as Legend } from '../components/home/mvvSuperior.jsx'
import { Projects as Tecnologies } from '../components/home/Projects.jsx'
import { Mision } from '../components/home/Mision.jsx'
import { SimpleBoxes01 as Team } from '../components/home/SimpleBoxes01.jsx'
import { Contact01 } from '../components/home/contact01.jsx'
import { Footer } from '../components/commons/Footer'
import { PlanSection } from '../components/home/PlanSection01.jsx'
import { Service03 } from '../components/home/Service03.jsx'

export const Home = () => {
  return (
    <div className='bg-[#eeeeee] dark:bg-[#282c34]'>
      <Header />
      <Hero /> 
      <Service02 />
      <Service03 />
      <Contact01 />
      {
        // <PlanSection />
        // <Legend />
        // <Tecnologies />
        // <Mision />
        // <Team />
        // <Contact01 />
      }    
      
      <Footer />
    </div>
  )
}
