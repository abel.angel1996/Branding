import Typewriter from 'typewriter-effect';


export const TextAnimated = (props) => { 
  return (
  <Typewriter 
      onInit={ 
        (typewriter) => {
          typewriter
            .pauseFor(1500)
            .typeString( props.children )
            .start()
        }
      } 
      options={{
        autoStart: true,
        loop: false,
        delay: 45
      }}
  />
  )
}
