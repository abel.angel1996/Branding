import React from 'react';

/**
 * Proposito: Titulo principal de la pagina. 
 * Dependencias: tailwind, text-plt-mc-txt-A
 * @param {*} props 
 * 
 */
export const T1 = (props) => {
    return (
        <h1 className='capitalize text-3xl md:text-[4vw] lg:text-5xl  leading-[1.375] text-center font-semibold text-plt-mc-title-A dark:text-plt-mo-title-A'>
            { props.children }
        </h1>
    );
};

export const T2 = (props) => {
    return (
        <h2 className='text-center font-semibold text-xl w-11/12 lg:w-3/5 text-plt-mc-title-A dark:text-plt-mo-title-A'>
            { props.children }
        </h2>
    );
};
