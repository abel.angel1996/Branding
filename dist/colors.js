export let colores = 
{
    'palette-light-bg100':'#F1F5F9',
    'palette-light-bg200':'#E2E8F0',
    'palette-light-bg300':'#CBD5E1',
    'palette-light-bg400':'#94A3B8',
    'palette-light-txt800':"#1E293B",
    'palette-light-txt700':"#334155",
    'palette-color':"rgb(8,126,164)",
    'palette-color02':"#2e3f51" //el original del footer
}