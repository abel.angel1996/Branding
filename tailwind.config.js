/** @type {import('tailwindcss').Config} */
import { colores } from './public/colors'

export default {
  content: [
    './index.html',
    './src/**/*.{js,ts,jsx,tsx}'
  ],
  theme: {
    extend: {
      colors: {
        bgColor: 'var(--bg-color)',
        bgColorContainer: 'var(--bg-color-container)',
        txtColor: 'var(--txt-color)',
        ...colores
      },
      keyframes: {
        slideOutLeft: {
          '0%, 100%': { transform: 'translateX(0)' },
          '0%': { transform: 'translateX(100vw)' }
        },
        slideInLeft: {
          '100%': { transform: 'translateX(100vw)' },
          '100%, 0%': { transform: 'translateX(0)' }
        }
      },
      animation: {
        slideOutLeft: 'slideOutLeft 400ms ease-in-out',
        slideInLeft: 'slideInLeft 400ms ease-in-out'
      },
      fontFamily: {
        sans: ['sans-serif']
      }
    }
  },
  plugins: [],
  darkMode: 'class'
}
